<?php

/**
 * @file
 * Drupher's system settings form.
 */

/**
 * Drupher settings form. system_settings_form() callback.
 */
function drupher_settings() {
  $form = array(
    'drupher_on' => array(
      '#type' => 'checkbox',
      '#title' => t('Drupher is enabled'),
      '#description' => t('When Drupher is disabled, connections to the Gopher server will still work (assuming the daemon is operating); however, no site content will be served.'),
      '#default_value' => variable_get('drupher_on', FALSE),
      '#weight' => 0,
    ),
    'drupher_default_path' => array(
      '#type' => 'textfield',
      '#title' => t('Default path'),
      '#description' => t('Enter the path to the default node to be shown when the visitor does not request a particular path.'),
      '#weight' => 10,
      '#default_value' => variable_get('drupher_default_path', ''),
    ),
/*
    'drupher_banner' => array(
      '#type' => 'textarea',
      '#title' => t('Index banner'),
      '#rows' => 6,
      '#description' => t('This text will be shown at the top of the directory listing when the main index of the Gopher server is requested.'),
      '#default_value' => variable_get('drupher_banner', ''),
      '#weight' => 20,
    ),
    'drupher_banner_format' => filter_form(variable_get('drupher_banner_format', FILTER_FORMAT_DEFAULT)),
    'drupher_breadcrumbs' => array(
      '#type' => 'checkbox',
      '#title' => t('Show breadcrumbs'),
      '#description' => t('When selected, the first menu elements in directory listings will be equivalent to Drupal&rsquo;s standard breadcrumbs.'),
      '#default_value' => variable_get('drupher_breadcrumbs', TRUE),
      '#weight' => 40,
    ),
*/
  );
  $form = system_settings_form($form);
  $form['buttons']['submit']['#weight'] = 1000;
  $form['buttons']['reset']['#weight'] = 1010;
  $form['buttons']['#weight'] = 1000;
  return $form;
  
}
#!/usr/bin/env php
<?php

// NOTE: Using /usr/bin/env php for the hashbang line above may not work
// properly; PHP may not load its normal php.ini config file, and your database
// drivers won't know where to properly find the sockets it needs to connect to
// to communicate with the database server. If this happens, alter the hashbang
// line above to point to your PHP binary directly.

/**
 * @file
 * Process a Gopher request.
 *
 * Parse the Gopher request, bootstrap Drupal, find the relevant menu or node
 * in Drupal's database, and serve it to the visitor.
 */

// Lazily parse CLI arguments. Due to limitations of inetd/launchd/etc, this is
// the only way to get good info about the environment into the script (I
// think). We're going to be lazy and only recognize flags we understand while
// ignoring others silently.

// Pop off item 0, the path to the script.
$my_path = array_shift($argv);
foreach ($argv as $arg) {
  if (preg_match('/^--(host|port)=[\'"]?([^\'"]+)/', $arg, $matches)) {
    if ($matches[1] === 'host') {
      define('GOPHER_HOST', $matches[2]);
    }
    elseif ($matches[1] === 'port') {
      define('GOPHER_PORT', $matches[2]);
    }
  }
}

if (!defined('GOPHER_HOST')) {
  define('GOPHER_HOST', 'error');
  define('GOPHER_PORT', '70');
  drupher_d_error('Host parameter not defined (identity crisis!)');
}
if (!defined('GOPHER_PORT')) {
  define('GOPHER_PORT', '70');
}

// Get the input
$query = trim(fgets(STDIN));

// Bootstrap Drupal. We're going to find the path to the Drupal installation
// by finding the full path to this script and finding the directory that's
// above the "sites" directory in that path. If that doesn't work, then the
// module is probably in the root "modules" directory (such as when Drupher gets
// accepted into core), so try the directory above the "modules" directory next.
if (preg_match('~^(.+)/sites/~', $my_path, $matches) || preg_match('~^(.+)/modules/~', $my_path, $matches)) {
  $drupal_dir = $matches[1];
}
else {
  drupher_d_error('Drupal directory not found.');
}

chdir($drupal_dir);

// Before we bootstrap Drupal, is the user requesting a file on disk?
/*
if (preg_match('~^sites/' . GOPHER_HOST . '/files/([^\.].+)$~', $query, $matches)) {
  // Check for sneaky arbitrary file requests.
  $realpath = realpath("{$drupal_dir}/{$query}");
  if (strpos($realpath, $drupal_dir . '/' . GOPHER_HOST . '/files/') !== 0 || strpos($realpath, '/.') !== FALSE) {
    drupher_t_error('Illegal file request.');
  }
  elseif (!file_exists($realpath)) {
    drupher_t_error('File not found.');
  }
  else {
    readfile($realpath);
    die();
  }
}
*/

// Set $_SERVER['HTTP_HOST'] so Drupal knows what site directory to look for
// settings.php in when it bootstraps.
$_SERVER['HTTP_HOST'] = GOPHER_HOST;
// The Drupal function ip_addresses() is going to cause an error if the
// $_SERVER['REMOTE_ADDR'] value isn't set, but we don't really know what that
// will be. So here's a fake value.
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

$_SERVER['REQUEST_METHOD'] = 'GET';
$_SERVER['SERVER_SOFTWARE'] = 'Drupher';

// Okay, now we're ready to bootstrap.
require_once($drupal_dir . '/includes/bootstrap.inc');

// Bootstrapping to DRUPAL_BOOTSTRAP_DATABASE gets us database access, but
// frustratingly variable_get() doesn't become useful (the $conf global doesn't
// get fully populated) until after DRUPAL_BOOTSTRAP_LATE_PAGE_CACHE. I haven't
// tried it yet, but I'll bet that the cache_* functions don't work before then
// too…
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Hooray, we've bootstrapped! Now we can use Drupal functions.
// So what are we doing?

// First, let's find out if we're online.
if (!variable_get('drupher_on', FALSE) || variable_get('site_offline', FALSE)) {
  drupher_d_error(t('This Drupher server is currently disabled.'));
/*   drupher_d_error('This Drupher server is currently disabled.'); */
}

if ($query === '') {
  // The default path is being requested. Get it.
  $selector = variable_get('drupher_default_path', '');
  if ($selector === '') {
/*     drupher_d_error(t('You have requested the index page for this server, but the server administrator has not yet defined one.')); */
    drupher_d_error('The server administrator has not yet defined a default path.');
  }
}
else {
  // Remove leading slash from query
  $selector = substr($query, 1);
}


// Find the requested node's NID.
if (!preg_match('~^node/(\d+)$~', $selector, $matches)) {
  $system = drupal_lookup_path('source', $selector);
  if (!preg_match('~^node/(\d+)$~', $system, $matches)) {
/*     drupher_d_error(t('The requested path is invalid: @path', array('@path' => $query))); */
    drupher_d_error('The requested path is invalid: ' . $query);
  }
}

// Load the node from the database, if present.
$node = node_load(array('nid' => $matches[1]));
if (!$node || !$node->status) {
    drupher_d_error('The requested path is invalid: ' . $query);
}
else {
  $dir = drupher_parse_gophermap($node->body);
  if ($dir === FALSE) {
    // It looks like there's no Gophermap data here.
    die(drupal_html_to_text(node_view($node)));
  }
  else {
    drupher_d_dir($dir);
  }
}

/**
 * Send an error and exit.
 * 
 * @param $response
 *   The error message. A generic error message will be sent if NULL.
 */
function drupher_d_error($message = NULL) {
  $response = array(
    'type' => '3',
    'display' => $message === NULL ? 'An error occurred.' : $message,
    'selector' => '/',
  );
  drupher_d_dir(array($response));
}

/**
 * Send a gopher directory listing and exit.
 *
 * @param $items
 *   An array of the "files" and "subdirectories" in the "directory" we're
 *   listing. Each item is an associative array with the following keys:
 *   type - The type of the item.
 *   display - The label of the item; such as the filename.
 *   selector - The path to the item.
 */
function drupher_d_dir($items) {
  $response = '';
  $defaults = array(
    'host' => GOPHER_HOST,
    'port' => GOPHER_PORT,
    'selector' => '/',
  );
  foreach ($items as $item) {
    $item = array_merge($defaults, $item);
    $response .= "{$item['type']}{$item['display']}\t{$item['selector']}\t{$item['host']}\t{$item['port']}\r\n";
  }
  die($response . '.');
}

/**
 * Gophermap parser.
 *
 * For more info on the Gophermap format, see:
 * gopher://gopher.floodgap.com/0/buck/dbrowse?faquse%201
 * http://en.wikipedia.org/wiki/Gophermap
 * This implementation varies a bit from the standard because we're going to
 * allow for lines of arbitrary length - and then split them down to 67-char
 * lines later. We're also going to allow the \ (backslash) character to be
 * used instead of a tab, since they're more visible than tabs.
 *
 * @param $text
 *   The text to parse.
 * @return
 *   The parsed Gopher directory listing, as an array; or FALSE if it doesn't
 *   appear that the text contains Gophermap data.
 */
function drupher_parse_gophermap($text, $cur_path = '/') {
  // First, test whether Gopher data can be found anywhere in the text
  if (!preg_match('/(*ANYCRLF)^[\w {1,3}]+\w(\t|    )[\w(\t|    )\/$]/m', $text)) {
    return FALSE;
  }
  else {
    $dir = array();
    // Split the text on line breaks.
    $lines = preg_split('/\r?\n/', $text);
    foreach ($lines as $line) {
      // Does it appear to have Gophermap data?
      // The preg functions don't allow overlapping matches (on "A A A", "/A A/"
      // will only match once), so we do a special case for two sequential tabs.
      if (!preg_match('/(*ANYCRLF)^(\w)([^\t]+)\t(\t|[^\t]+)?\t?([^\t]+)?\t?(\d+)?/', preg_replace(
        array('/(\S)        (\S|$)/', '/(\S)    (\S|$)/'),
        array("$1\t\t$2", "$1\t$2"),
      $line), $matches)) {
        // It doesn't look like it. So split it into 67-char lines and make them
        // info lines.
        if (strlen($line) <= 67) {
          $wrapped = array($line);
        }
        else {
          $wrapped = explode("\n", wordwrap($line, 67, "\n", TRUE));
        }
        foreach ($wrapped as $wrapped_line) {
          $dir[] = array(
            'type' => 'i',
            'display' => $wrapped_line,
          );
        }
      }
      else {
/*         var_dump($matches); */
        // Looks like this line *does* have Gophermap data. Parse it.
        $list = array(
          'type' => $matches[1],
          'display' => substr($matches[2], 0, 67),
        );
        if (!isset($matches[3])) {
          $list['selector'] = $matches[2];
        }
        else {
          $list['selector'] = $matches[3];
/*           $list['display'] .= " ({$matches[3]})"; */
          if (isset($matches[4])) {
            $list['host'] = $matches[4];
            if (isset($matches[5])) {
              $list['port'] = $matches[5];
            }
          }
        }
        if ($list['selector'] === '/' || $list['selector'] === "\t") {
          $list['selector'] = '';
        }
        elseif ($list['selector']{0} !== '/') {
          if ($cur_path === '/') {
            $list['selector'] = '/' . $list['selector'];
          }
          else {
            $list['selector'] = $cur_path . '/' . $list['selector'];
          }
        }
        $dir[] = $list;
      }
    }
/*     die(); */
    return $dir;
  }
}